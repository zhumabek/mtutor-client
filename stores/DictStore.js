import {action, observable} from 'mobx';
import * as request from '../utils/requester';
import {storageGet, storageSave} from "../utils/LocalStorage";


export default new class DictStore {

  @observable dicts = [];
  @observable dictItem = [];

  constructor() {
  }

  @action
  getList() {
    storageGet('dicts').then(r => {
      this.dicts = r;
    }).finally(() => {
      this.loadFromApi(false);
    });
  }

  async getDictData(tableName, page) {
    console.log(tableName, page);
    // TODO: fix limit, search is not working if searching object is not in limit
    let r = await request.post('dictionary.listing', {type: tableName, /*limit: 50,*/ offset: page});
    return r.docs || [];
  }

  @action
  loadFromApi() {
    request.post('/dictionary/tables_list').then(r => {
      this.dicts = r.tables || [];
      storageSave('dicts', r.tables || []);
    });
  }

  async saveDictData(tableName, dict) {
    let params = dict;
    params.type = tableName;
    await request.post('dictionary.save', params);
  }

  async deleteDictData(tableName, dict) {
    let params = dict;
    params.type = tableName;
    await request.post('dictionary.delete', params);
  }

  async getDictData2(params) {
    // TODO: fix limit, search is not working if searching object is not in limit
    let r = await request.post('dictionary.listing', params);
    return r.docs || [];
  }
  async getData(params){
    let r = await request.post('dircoate/listing', params);
    return r.docs || [];

  }

   async remove(id) {
    await request.post('dircoate/remove', {id});

  }

  getCoate = id => request.postAsync("dircoate/find", "docs", {id});
  saveCoate = request.postAsync.bind(null, "dircoate/update", null);
  put = request.postAsync.bind(null, 'dircoate/save', null)



}
