import {action, computed, observable} from 'mobx';
import * as request from '../utils/requester';


export default new class AdminStore {

  @observable menu;
  @observable menus = [];

  @observable role;
  @observable roles = [];

  @observable api;
  @observable apis = [];

  constructor() {
    this.resetMenu();
    this.resetRole();
  }

  @action
  getMenus() {
    this.resetMenu();
    request.get('menu/list').then(r => {
      this.menus = r.docs || [];
    })
  }

  @action
  saveMenu() {
    request.post('menu/put', this.menu).then(r => {
      this.getMenus();
    })
  }

  @action
  removeMenu() {
    request.remove('menu/remove/'+this.menu.id, {}).then(r => {
      this.getMenus();
    })
  }

  @action
  saveRole() {

    request.post('role/put', this.role).then(r => {
      this.getRoles();
    })
  }

  @computed
  get canSaveMenu() {
    return this.menu.name && this.menu.url;
  }

  @computed
  get canSaveRole() {
    return this.role.name;
  }

  @action
  setMenu(menu) {
    if (menu) {
      this.menu = menu;
    } else {
      this.resetMenu();
    }
  }

  @action
  resetMenu() {
    this.menu = {
      name: '',
      parent_id: null,
      url: '',
      order: null,
      role: null,
      active: true,
      roles_id: []
    }
  }

  @action
  getRoles(params) {
    this.resetRole();
    request.get('role/list').then(r => {
      this.roles = r.docs || [];
    })
  }

  @action
  setRole(role) {
    if (role) {
      this.role = role;
    } else this.resetRole();
  }

  @action
  resetRole() {
    this.role = {name: '', parent_id: null, menus_id: [], code: '', role: 0}
  }

  @action
  async statusConfirmed(param) {
    let r = await request.post('company/status_confirmed', param);
  }

  @action
  async statusRejected(param) {
    let r = await request.post('company/status_rejected', param);
  }

  @action
  async statusBlacklist(param) {
    let r = await request.post('company/status_blacklist', param);
  }

  @action
  async statusBlocked(param) {
    let r = await request.post('company/status_blocked', param);
  }


  getAllSubjects = request.getAsync.bind(null, `subject/list`, "docs");
  getApplicants = params => request.postAsync(`user/applicants`, "docs", params);
  addSubjects = params => request.postAsync("subject/add", 'doc', params);
  deleteSubject = id => request.deleteAsync( `subject/delete/${id}`, 'doc');
  getSubject = id => request.getAsync( `subject/get/${id}`, 'doc');
  updateSubject = (id, params) => request.putAsync( `subject/update/${id}`, 'doc', params);
  getUsers = request.postAsync.bind(null, "user/userlist", "users");
  getUser = id => request.getAsync(`user/get/${id}`, "user", {});
  updateUserStatus = params => request.postAsync('user/update/status', "doc", params);
  findTutors = id => request.getAsync(`user/tutors/${id}`, "docs", {});

}
