import {action, computed, observable, observe} from "mobx";
import * as request from "../utils/requester";
import {showInfo} from '../utils/messages';
import {storageGet, storageSave, storageRemove} from "../utils/LocalStorage";
import menuStore from './MenuStore';

const STATE_INIT = 0; // initital state
const STATE_PENDING = 1; // auth in progress
const STATE_READY = 2; // auth finished


export default new class AuthStore {

  @observable state = STATE_INIT;
  @observable token;
  @observable valid = false;
  @observable user;
  @observable notification;
  @observable isTutor;
  @observable isStudent;

  checkNotification;

  constructor() {
    observe(this, 'valid', change => {
      if (this.valid) {
        if (!this.checkNotification) {
          this.checkNotification = setInterval(() => {
            this.getNotification();
          }, 60000);
        }
      } else {
        clearInterval(this.checkNotification);
      }
    });
  }

  @action
  getNotification() {
    // request.post('notification.listing', {}, true).then(r => this.notification = r);
  }

  @computed
  get isReady() {
    return this.state === STATE_READY
  }

  // TODO: actions cannot be async

  // check auth
  async check(force) {

    if (this.state !== STATE_INIT && !force) return;

    if (!force) {
      this.state = STATE_PENDING;
    }
    let token = await storageGet('token');
    let user = await storageGet('user');
    if (token && user) {
      this.token = token;
      this.setUser(user);
      this.isStudent = user.role && user.role === 5;
      this.isTutor = user.role && user.role === 8;
    } else {
      this.reset();
    }

    await menuStore.load();

    this.state = STATE_READY;
  }

  @action
  setData(data) {
    if (data.user) {
      this.setUser(data.user);
      this.valid = true;

      if (data.token) {
        this.token = data.token;
        storageSave('token', data.token);
        storageSave('user', data.user);
      }

      if (data.user) {
        this.isStudent = data.user.role && data.user.role === 5;
        this.isTutor = data.user.role && data.user.role === 8;
      } else {
        this.isStudent = false;
        this.isTutor = false;
      }

    } else {
      this.reset()
    }
  }

  @action
  setUser(user) {
    if (!user)
      this.reset();

    this.valid = true;
    this.user = user;
  }

  @action
  updateUser(user) {
    this.valid = false;
    if (!user)
      this.reset();
    // storageRemove('user');
    storageSave('user', user)
    this.valid = true;
    this.user = user;
  }

  @action
  setRole(role) {
    if (!role)
      this.reset();

    // this.valid = true;
    this.user.role = role;
  }


  @action
  reset() {
    this.token = undefined;
    this.user = undefined;
    this.valid = false;
    this.isTutor = false;
    this.isStudent = false;
    storageRemove('token');
    storageRemove('user');
  }

  async login(username, password) {
    let r = await request.post('auth/login', {username, password});
    if (r.token) {
      this.setData(r);
      this.getNotification();
      await menuStore.load();
      return true;

    } else {
      console.warn('no token!');
    }
  }

  logout() {
    this.reset();
    menuStore.reset()
    menuStore.load();
  }


  // COMPANY


  // REG

  async register(passwordData) {
    let params = this.user;
    params.password = passwordData.password;
    params.confirm_password = passwordData.password_confirmation;
    let r = await request.put('auth/signup', params);
    this.setData(r);
    this.check(true);
    showInfo('Вы успешно прошли регистрацию');
  }

  async sendSmsCode(params) {
    return await request.post('user/send_otp', params);
  }

  async validateSmsCode(phone, otp) {
    phone = phone.replace('(', '').replace(')', '').replace(' ', '');
    return await request.post('user/validate_otp', {phone, otp});
  }

  // SAVE

  async uploadAvatar(file) {
    return await request.post('upload/avatars', {file});
  }

  async save(params) {
    return await request.post('user/put', params);
  }

  async saveData(data) {
    return await request.post('user/putData', {data});
  }

  async changePassword(params) {
    return await request.post('user/putPassword', params);
  }

  // RECOVERY

  async recover(phone) {
    return await request.post('password/recovery/sendCode', {login: phone});
  }

  async recoveryPassword(passwordData) {
    let params = this.user;
    params.password = passwordData.password;
    params.confirm_password = passwordData.confirmPassword;
    let r = await request.post('user/recovery_password', params);
    this.setData(r);
  }

  async checkData(params) {
    return await request.post('user/check', params);
  }
}
