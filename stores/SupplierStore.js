import {action, observable} from "mobx";
import * as request from "../utils/requester"
import moment from "moment";

export default new class SupplierStore {

  // test data

  // getEmployees = request.postAsync.bind(null, "employee/listing", "docs");


  @action
  async uploadFile(file) {
    let r = await request.post('prequalification/upload/file', file);
    return r;
  }

  @action
  async saveApplicationProfile(params) {
    let r = await request.post('prequalification/profile/save', params);
    return r;
  }


  @action
  async saveApplicationCourseOffer(params) {
    let r = await request.post(`prequalification/course/save`, params);
    return r;
  }

  @action
  async saveSchedule(params) {
    let r = await request.post(`schedule/save`, params);
    return r.doc;
  }

  @action
  async getSchedule(id) {
    let r = await request.get(`schedule/get/${id}`, {});
    return r.doc;
  }

  @action
  async saveApplicationSchedule(params) {
    let r = await request.post(`prequalification/schedule/save`, params);
    return r;
  }



  async getApplications(params) {

  }
}
