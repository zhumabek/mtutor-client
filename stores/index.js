import mainStore from "./MainStore";
import authStore from "./AuthStore";
import menuStore from "./MenuStore";
import dictStore from "./DictStore";
import adminStore from "./AdminStore";
import supplierStore from "./SupplierStore"
import notificationsCtrl from './controllers/Messages/NotificationsCtrl';
import newsCtrl from './controllers/Messages/NewsCtrl';

export default {
  mainStore,
  authStore,
  menuStore,
  dictStore,
  adminStore,
  supplierStore,

  // common controllers
  notificationsCtrl,
  newsCtrl,

  // student


  // supplier

};
