import axios from 'axios';
import mainStore from '../stores/MainStore';
import authStore from '../stores/AuthStore';
import {hideMessage, showError, showSuccess} from './messages';

import {getApi, isDevMode} from "./common";

const CONNECTION_FAILURE = 'Ошибка соединения';
const REQUEST_TIMEOUT = 'Время запроса истекло';
const SYSTEM_ERROR = 'Системная ошибка';
const NETWORK_ERROR = 'Ошибка связи';

const API = getApi();

const instance = axios.create({
  baseURL: API,
  timeout: 30000,
  responseType: 'json',
  headers: {},
});

// instance.defaults.headers.post['Content-Type'] = 'multipart/form-data';

let deviceId;

// interceptor before request
instance.interceptors.request.use(function (config) {
  deviceId = deviceId || mainStore.getDeviceId();

  config.headers = {'Authorization': `Bearer ${authStore.token}`};
  if (isDevMode())
    console.log('REQUEST:', config.url, config);
  hideMessage();
  return config;
});

// interceptor after response
instance.interceptors.response.use(function (response) {
  mainStore.setBusy(false);
  if (isDevMode()) {
    console.log('RESPONSE:', response.config.url, response);
    if (response.status === 200 || response.status === 201) {
      // mainStore.showLogin();
      // TODO: mainStore.logout();
      showSuccess(response.data.message || "Successfully fetched!!")
    }
    else {
      showError(response.data.message || SYSTEM_ERROR);
    }
  }

  return response;
}, function (error) {
  mainStore.setBusy(false);
  return Promise.reject(error);
});

export function get(url, param, silent) {
  if (!silent)
    mainStore.setBusy(true);

  return new Promise(function (resolve, reject) {
    instance.get(url, {params: param})
      .then(resp => {
        if (resp && resp.data) {
          resolve(resp.data);
        }
        else {
          reject(null);
        }
      })
      .catch(error => {
        if (!silent) {
          let msg = `${error.message}. ${error.response.data.message}` || CONNECTION_FAILURE;
          if (msg.includes('timeout')) {
            msg = REQUEST_TIMEOUT;
          }
          if (msg.includes('malformed')) {
            msg = "You are not authorized!!";
          }
          if (msg.includes('expired')) {
            msg = "Your session is expired!!";
            authStore.logout()
          }
          if (msg.toLowerCase().includes('network error')) {
            msg = NETWORK_ERROR;
          }
          showError(msg);
        }
        reject(error);
      });
  });
}

export function post(url, param, silent) {
  if (!silent)
    mainStore.setBusy(true);
  param = param || {};
  param.client = mainStore.getClient();
  param.lang = mainStore.language.code;
  return new Promise(function (resolve, reject) {
    instance.post(url, param).then(resp => {
      if (resp && resp.data) {
        if (resp.status === 200 || resp.status === 201) {
          resolve(resp.data);
        }
        else {
          reject(resp.data)
        }
      }
      else {
        reject(null);
      }
    }).catch(error => {
      if (!silent) {
        let msg = `${error.message}. ${error.response.data.message}` || CONNECTION_FAILURE;
        if (msg.includes('timeout')) {
          msg = REQUEST_TIMEOUT;
        }
        if (msg.toLowerCase().includes('network error')) {
          msg = NETWORK_ERROR;
        }
        showError(msg);
      }
      reject(error);
    });
  });
}
export function put(url, param, silent) {
  if (!silent)
    mainStore.setBusy(true);
  param = param || {};
  param.client = mainStore.getClient();
  param.lang = mainStore.language.code;
  return new Promise(function (resolve, reject) {
    instance.put(url, param).then(resp => {
      if (resp && resp.data) {
        if (resp.status === 200 || resp.status === 201) {
          resolve(resp.data);
        }
        else {
          reject(resp.data)
        }
      }
      else {
        reject(null);
      }
    }).catch(error => {
      if (!silent) {
        let msg = error.message || CONNECTION_FAILURE;
        if (msg.includes('timeout')) {
          msg = REQUEST_TIMEOUT;
        }
        if (msg.toLowerCase().includes('network error')) {
          msg = NETWORK_ERROR;
        }
        showError(msg);
      }
      reject(error);
    });
  });
}

export function remove(url, param, silent) {
  if (!silent)
    mainStore.setBusy(true);

  return new Promise(function (resolve, reject) {
    instance.delete(url, {params: param})
      .then(resp => {
        if (resp && resp.data) {
          resolve(resp.data);
        }
        else {
          reject(null);
        }
      })
      .catch(error => {
        reject(error);
      });
  });
}


export async function getAsync(url, propertyName, param) {
  let r = await get(url, param);
  return propertyName ? r[propertyName] : r;
}

export async function deleteAsync(url, propertyName, param) {
  let r = await remove(url, param);
  return propertyName ? r[propertyName] : r;
}

export async function postAsync(url, propertyName, param) {
  let r = await post(url, param);
  return propertyName ? r[propertyName] : r;
}

export async function putAsync(url, propertyName, param) {
  let r = await put(url, param);
  return propertyName ? r[propertyName] : r;
}

// export default instance;
