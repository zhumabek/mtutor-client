const webpack = require('webpack');
const path = require('path');
const ReactWebConfig = require('react-web-config/lib/ReactWebConfig').ReactWebConfig;
const envFilePath = path.resolve(__dirname, '.env');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const noop = require('noop-webpack-plugin');
const SplitByPath = require('webpack-split-by-path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const extractSCSS = new ExtractTextPlugin('[name].css');

const BUILD_DIR = path.resolve(__dirname, './web/build');
const SRC_DIR = path.resolve(__dirname, './web/src');
const PORT = 8080;

module.exports = (env = {}) => {
  const extractSCSSUse = extractSCSS.extract({
    fallback: 'style-loader', // inject CSS to page
    use: [
      {
        loader: 'css-loader', // translates CSS into CommonJS modules
        options: {
          sourceMap: !env.prod,
        }
      },
      {
        loader: 'sass-loader',
        options: {
          sourceMap: !env.prod
        }
      }
    ]
  });

  return {
    entry: {
      main: ['babel-polyfill', SRC_DIR + '/index.js']
    },
    output: {
      path: BUILD_DIR,
      filename: env.prod ? '[name].[chunkhash].js' : '[name].js',
      chunkFilename: env.prod ? '[name].[chunkhash].js' : '[name].js',
    },
    devtool: env.prod ? false : 'eval-source-map',
    devServer: {
      port: PORT,
      compress: true,
      hotOnly: true,
      inline: true,
      open: true
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: path.join(__dirname, 'node_modules'),
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            presets: ['stage-0', 'react', 'env'],
            plugins: [
              "transform-decorators-legacy",
              "transform-class-properties",
              [
                "transform-imports",
                {
                  "lodash-es": {
                    "transform": "lodash-es/${member}",
                    "preventFullImport": true
                  },
                  "reactstrap": {
                    "transform": "reactstrap/lib/${member}",
                    "preventFullImport": true
                  },
                  "recompact": {
                    "transform": "recompact/${member}",
                    "preventFullImport": true
                  },
                  "react-router-dom": {
                    "transform": "react-router-dom/es/${member}",
                    "preventFullImport": true
                  }
                }
              ],
            ],
            env: {
              production: {
                plugins: [
                  'transform-remove-console'
                ]
              }
            }
          },
        },
        {
          test: /\.es6$/,
          exclude: /node_modules/,
          loader: 'babel',
          query: {
            presets: ['es2015']
          }
        },
        {
          test: /\.(js|jsx)$/,
          include: [
            path.join(__dirname, 'node_modules/react-native-storage')
          ],
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            presets: ['env', 'stage-1', 'react'],
          }
        },
        {
          test: /\.html$/,
          loader: 'html-loader'
        },
        {
          test: /\.(css|scss)$/,
          use : env.prod ?
            extractSCSSUse :
            ['css-hot-loader'].concat(extractSCSSUse)
        },
        {
          test: /\.(png|jpg|jpeg|gif|ico)$/,
          use: [
            {
              // loader: 'url-loader'
              loader: 'file-loader',
              options: {
                name: './img/[name].[hash].[ext]'
              }
            }
          ]
        },
        {
          test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'file-loader',
          options: {
            name: './fonts/[name].[hash].[ext]'
          }
        }]
    },
    plugins: [
      new SplitByPath([{
        name: 'vendor',
        path: path.join(__dirname, 'node_modules/'),
      }]),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(env.prod ? 'production' : 'development')
        }
      }),
      env.prod ? new UglifyJsPlugin({
        parallel: true,
        uglifyOptions: {
          compress: false
        },
        cache: true
      }) : noop(),
      env.prod ? noop() : new webpack.NamedModulesPlugin(),
      env.prod ? noop() : new webpack.HotModuleReplacementPlugin(),
      extractSCSS,
      new HtmlWebpackPlugin(
        {
          inject: true,
          template: './web/public/index.html'
        }
      ),
      new CopyWebpackPlugin([
          {from: './web/public/img', to: 'img'}
        ],
        {copyUnmodified: false}
      ),
      ReactWebConfig(envFilePath)
    ],
    resolve:{
      alias: {
        'react-native-config': 'react-web-config',
        utils: path.resolve(__dirname, 'utils'),
        components: path.resolve(SRC_DIR, 'components'),
        controllers: path.resolve(__dirname, 'controllers'),
        stores: path.resolve(__dirname, 'stores'),
      }
    }
  }
};
