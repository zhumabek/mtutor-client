import React, {Component} from 'react';
import {inject, observer} from "mobx-react";
import {Card, CardBody, Col, Container, FormText, Row} from "reactstrap";
import Button from "components/AppButton";
import Input, {FGI} from "components/AppInput";
import SmsCodeInput from "components/SmsCodeInput";
import TelInput from "components/TelInput";
import {withRouter} from "react-router-dom";
import {showInfo} from 'utils/messages';

@inject('authStore') @withRouter @observer
export default class Recovery extends Component {

  constructor(props) {
    super(props);
    this.state = {email: null, phoneNumber: null, smsCode: null, isCountingDown: false};
  }

  render() {
    let {authStore} = this.props;

    return (
      <Container>
        <Row className={"justify-content-center"}>
          <Col md={"9"}>
            <Card>
              <CardBody className="p-4">
                <h2 className="mb-4"> Восстановление</h2>

                <Row className={"mb-2"}>
                  <Col md="6">
                    <FGI l="Email" lf="5" ls="7">
                      <Input type="email"
                             value={this.state.email}
                             onChange={(elem) => {
                               this.setState({email: elem.target.value})
                             }}/>
                      <FormText color="muted">Укажите Электронную почту</FormText>
                    </FGI>
                  </Col>
                </Row>

                <Row className={"mb-2"}>
                  <Col md="6">
                    <FGI l="Моб. телефон" lf="5" ls="7">
                      <TelInput value={this.state.phoneNumber}
                                onChange={phoneNumber => this.setState({phoneNumber})}/>
                      <FormText color="muted">Укажите зарегистрированный на ваше имя номер</FormText>
                    </FGI>
                  </Col>

                  <Col md="6">
                    <Button size="sm"
                            onClick={() => {
                              let params = {email: this.state.email, phone: this.state.phoneNumber};
                              authStore.sendSmsCode(params).then(r => {
                                showInfo('На Ваш мобильный телефон отправлено смс');
                                if (!this.state.isCountingDown)
                                  this.setState({isCountingDown: true});
                              });
                            }}
                            disabled={!this.canSendSmsCode()}>
                      Прислать СМС с кодом
                    </Button>
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <SmsCodeInput lf="5" ls="7"
                                  value={this.state.smsCode}
                                  callback={(value) => {
                                    this.setState({smsCode: value});
                                  }}
                                  disabled={!this.state.isCountingDown}
                                  isCountingDown={this.state.isCountingDown}
                                  onTimerComplete={() => {
                                    // TODO: Countdown has competed, what should we do?
                                    window.location.reload()
                                  }}>
                    </SmsCodeInput>
                  </Col>
                </Row>
                <Row>
                  <Col md="6">
                    <FGI l="" lf="5" ls="7">
                      <Button block onClick={() => {
                        authStore.validateSmsCode(this.state.phoneNumber, this.state.smsCode).then(r => {
                          let userData = {
                            email: this.state.email,
                            phone: this.state.phoneNumber
                          };
                          authStore.setUser(userData);

                          if (r.result === 0)
                            this.props.history.push('/recovery/confirm')
                        });
                      }}
                      >
                        Подтвердить
                      </Button>
                    </FGI>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }

  canSendSmsCode() {
    const {email, phoneNumber} = this.state;
    if (email && phoneNumber) {
      // TODO : is it done right ?
      return phoneNumber.length === 13 && !phoneNumber.includes("_");
    }
    return false
  }
};
