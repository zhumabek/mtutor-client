import React, {Component} from 'react';
import {Card, CardBody, CardFooter, CardHeader, Col, ListGroup, ListGroupItem, Row} from "reactstrap";
import Table from "components/AppTable";
import Button from "components/AppButton";
import {inject, observer} from "mobx-react";
import {FormInput} from "components/AppInput";
import Input from "components/AppInput";
import Select from "components/Select";
import Selector from "components/Select";
import Switcher from "components/Switcher";


@inject('adminStore') @observer
export default class Roles extends Component {

  componentDidMount() {
    this.props.adminStore.getRoles();
    this.props.adminStore.getMenus();
  }

  render() {
    const {adminStore} = this.props;
    const {role, roles, menus} = adminStore;

    return (
      <div className="animated fadeIn">
        <Card>
          <CardHeader>
            Roles
          </CardHeader>
          <CardBody>
            <Row>
              <Col xs="12" sm="6" lg="6">
                {roles.map(p =>
                  <Card key={p.id}>
                    <CardHeader onClick={() => {
                      adminStore.setRole(p);
                      this.setState({parent: null});
                    }}>
                      {p.name}
                    </CardHeader>
                    <CardBody>
                      <ListGroup>
                        {roles.filter(m => m.parent_id === p.id).map(m =>
                          <ListGroupItem color="info" key={m.id} tag="button" action
                                         onClick={() => adminStore.setRole(m)}>
                            {m.name}
                          </ListGroupItem>
                        )}
                      </ListGroup>

                    </CardBody>
                  </Card>
                )}
              </Col>
              <Col xs="12" sm="6" lg="6">
                <Card>
                  <CardHeader>
                    Роль
                    <div className="card-actions">
                      <Button onClick={() => adminStore.setRole()}>
                        <i className="fa fa-plus"/>
                      </Button>
                    </div>
                  </CardHeader>
                  <CardBody>
                    <FormInput label='parent'>
                      <Selector options={roles.slice()}
                              value={role.parent_id && role.parent_id.toString()}
                              labelKey='name'
                              valueKey='_id'
                              simpleValue
                              onChange={(val) => role.parent_id = val}/>
                    </FormInput>
                    <FormInput label='Меню'>
                      <Selector options={menus.slice()}
                                value={role.menus_id && role.menus_id.toString()}
                                labelKey='name'
                                valueKey='id'
                                multi
                                simpleValue
                                onChange={(val) => role.menus_id = val}/>
                    </FormInput>
                    <FormInput label='Название'><Input model={role} name='name'/></FormInput>
                    <FormInput label='Code'><Input model={role} name='code'/></FormInput>
                    <FormInput label='Role'><Input type={'number'} model={role} name='role'/></FormInput>
                  </CardBody>
                  <CardFooter>
                    <Button onClick={() => adminStore.saveRole()} disabled={!adminStore.canSaveRole}>Save</Button>
                  </CardFooter>
                </Card>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </div>
    );
  }
}
