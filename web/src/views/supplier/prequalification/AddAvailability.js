import React, {Component} from "react"
import {inject, observer} from "mobx-react"
import {Link} from "react-router-dom";
import {translate} from "react-i18next";
import {
  Card,
  Col,
  Container,
  Row
} from "reactstrap";
import moment from 'moment';
import Calendar from "../../../components/bigCalendar/Calendar";
import AppButton from "../../../components/AppButton";
const DEFAULT_DATE = "23-09-2019";
const min = moment(DEFAULT_DATE, 'DD-MM-YYYY').hours(8).toDate()
const max = moment(DEFAULT_DATE, 'DD-MM-YYYY').hours(20).toDate()

console.log(min,max)

@translate(['common', 'settings', ''])
@inject("supplierStore", "authStore", "adminStore") @observer
export default class AddAvailability extends Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      defaultDate: moment(DEFAULT_DATE, "DD-MM-YYYY").toDate(),
      scheduleId: null
    }
  }

  componentDidMount() {
    this.props.adminStore.getUser(this.props.authStore.user.id).then(r => {
      if(r.scheduleId){
        this.props.supplierStore.getSchedule(r.scheduleId)
          .then(r => {
            this.loadEvents(r.events)
          })
      }
    })
  }

  loadEvents(events){
    const parsedEvents = events.map(event => {
      event.start = moment(event.start).toDate()
      event.end = moment(event.end).toDate()
      return event
    })
    this.setState({events: parsedEvents})
  }

  handleSelect = (slot) => {
    this.setState({
      events: [
        ...this.state.events,
        {
          id: this.props.authStore.user.id + moment().format("DD-MM-YYYY HH:mm:ss"),
          start: slot.start,
          end: slot.end,
        },
      ],
    })
  }

  handleOnDoubleClickEvent = ({id}) => {
    let events = [...this.state.events];
    events.forEach((e, index) => {
      if(e.id === id){
        events.splice(index, 1)
      }
    })
    this.setState({events})
  }

  canSave() {
    let {events, scheduleId} = this.state;
    return events;
  }

  save = () => {
    this.props.supplierStore.saveSchedule({
      events: this.state.events,
      scheduleId: this.state.scheduleId
    })
      .then(r => {
        this.props.supplierStore.saveApplicationSchedule({
          userId: this.props.authStore.user.id,
          scheduleId: r.id
        })
          .then(r => this.props.history.push('/supplier/qualification'))
      })
  }

  render() {
    const {t} = this.props;
    return (
      <Row>
        <Col xs={12}>
          <Card style={{height: '100%'}}>
            <Calendar
              events={this.state.events}
              view={'week'}
              toolbar={false}
              defaultDate={this.state.defaultDate}
              selectable
              defaultView={'week'}
              onDoubleClickEvent={this.handleOnDoubleClickEvent}
              onSelectSlot={this.handleSelect}
              min={min}
              max={max}
            />
          </Card>
        </Col>
        <Col xs={12}>
          <AppButton color='danger' onClick={() => this.props.history.goBack()}>{'Отмена'}</AppButton>
          {' '}
          <AppButton color='primary' onClick={this.save} disabled={!this.canSave()}>{'Сохранить'}</AppButton>
        </Col>
      </Row>

    )
  }
}
