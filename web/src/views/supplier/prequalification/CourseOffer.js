import React, {Component} from 'react';
import {inject, observer} from "mobx-react";
import {
  Card,
  CardBody,
  Col,
  Row,
  CardTitle, CardHeader,
} from "reactstrap";
import {FGI} from "../../../components/AppInput";
import Input from "../../../components/AppInput";
import AppButton from "components/AppButton";
import Select from "components/Select";

@inject('supplierStore', 'authStore', 'adminStore') @observer
export default class CourseOffer extends Component {
  state = {
    subjects: [],
    subject_id: null,
    price: 0,
    user_id: this.props.authStore.user.id
  };

  componentWillMount() {
    this.props.adminStore.getAllSubjects().then(r => {this.setState({subjects: r})});
    this.props.adminStore.getUser(this.state.user_id).then(r => this.setState({subject_id: r.subjectId, price: r.price}));
  }

  canSave() {
    let {subject_id, price} = this.state;
    return subject_id && price;
  }

  save = () => {
    let {subject_id, price, user_id} = this.state;
    console.log("subject_id", subject_id)
    let params = {
      subject_id,
      price,
      user_id
    }
    this.props.supplierStore.saveApplicationCourseOffer(params).then(r => this.props.history.push('/supplier/qualification'))
  }

  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>{'Выбор курса преподования'}</CardTitle>
        </CardHeader>
        <CardBody>
          <Row>
            <Col xs={12}>
              <FGI l='Пожалуйста выберите курс вы собираетесь преподовать' lf="3" ls="8" className="my-1" required>
                <Select options={this.state.subjects} placeholder={'Выберите'}
                        valueKey="id" labelKey="name" value={this.state.subject_id}
                        onChange={subject_id => this.setState({subject_id: subject_id.id})}/>
              </FGI>
              <FGI l='Цена за час(60мин) занятия в Сомах' lf="3" ls="8" className="my-1" required>
                <Input type='number' value={this.state.price}
                       min={0}
                       onChange={e => this.setState({price: e.target.value})}/>
              </FGI>
            </Col>
          </Row>
          <Row>
            <Col xs={12} className="my-2">
              <AppButton color='danger' onClick={() => this.props.history.goBack()}>{'Отмена'}</AppButton>
              {' '}
              <AppButton color='primary' onClick={this.save} disabled={!this.canSave()}>{'Сохранить'}</AppButton>
            </Col>
          </Row>
        </CardBody>
      </Card>
    )

  }
}
