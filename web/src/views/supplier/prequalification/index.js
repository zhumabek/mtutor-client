import React, {Component} from "react"
import {translate} from "react-i18next";
import openSocket from 'socket.io-client'
import Hoc from "components/Hoc";
import {DEFAULT_AVA, getApi, IMAGES_URL} from "utils/common";
import {inject, observer} from "mobx-react";
import {Col, Container, DropdownItem, Row} from "reactstrap";
import {NavLink} from "react-router-dom";
import AppButton, {ConfirmButton} from "../../../components/AppButton";
import {showSuccess} from "../../../../../utils/messages";

@translate(['common', 'settings', ''])
@inject('authStore', 'adminStore') @observer
export default class SupplierQualification extends Component {
  constructor(...args) {
    super(...args);

    let {user} = this.props.authStore;

    this.state = {
      profile_status: false,
      schedule_status: false,
      course_status: false,
      payment_status: false,
      fullname: user.fullname,
      avatarPreview: user.data && user.data.avatar_img ? (IMAGES_URL + user.data.avatar_img) : DEFAULT_AVA
    };
  }

  componentDidMount() {
    this.props.adminStore.getUser(this.props.authStore.user.id).then(r => {
      let {
        fullname,
        birth_date,
        inn,
        education_name,
        education_faculty,
        education_degree,
        description_info,
        passport_series,
        passport_number,
        passport_given_date,
        passport_given_organ,
        transcript,
        subjectId, scheduleId, payment
      } = r;
      if (fullname &&
        birth_date &&
        inn &&
        education_name &&
        education_faculty &&
        // education_degree &&
        description_info &&
        passport_series &&
        passport_number &&
        passport_given_date &&
        passport_given_organ &&
        transcript) {
        this.setState({profile_status: true})
      }
      if (scheduleId) {
        this.setState({schedule_status: true})
      }
      if (subjectId) {
        this.setState({course_status: true})
      }
      if (payment) {
        this.setState({payment_status: true})
      }
    })
    const socket = openSocket(getApi());
    socket.on('applicants/confirmed/'+this.props.authStore.user.id, data => {
      this.props.authStore.updateUser(data.user);
      this.props.history.push('/home');
    })
  }

  canSave() {
    let {profile_status, course_status, schedule_status, payment_status} = this.state
    return profile_status && course_status && schedule_status && payment_status;
  }

  save() {
    let params = {
      userId: this.props.authStore.user.id,
      newStatus: 'pending'
    }
    this.props.adminStore.updateUserStatus(params).then(r => {
      showSuccess("Ваша запрос успешно отправлен!!!")
    })
  }

  render() {
    const {t} = this.props;
    return (
      <Hoc>
        <Container>
          <Row>
            <Col md={12} sm={12} xs={12}>
              <div>
                <h4 className="text-primary">{t('Здравсвтуйте ')}{this.state.fullname},</h4>
                <p
                  className="text-primary">{t("You're few steps away from becoming a tutor. We only need you to provide us with the  following information")}</p>
              </div>
            </Col>

          </Row>
          <ul className="list-group pt-3">
            <li onClick={() => this.props.history.push('/supplier/profile')}
                className="list-group-item list-group-item-primary d-flex justify-content-between align-items-center">
              <h6>{t('Заполните ваш профиль')}</h6>
              <div>
                <span className="fa fa-user"/>
                {this.state.profile_status ? <span className="px-2 text-success fa fa-check"/> :
                  <span className="px-2 text-danger fa fa-times"/>}
              </div>
            </li>
            <li onClick={() => this.props.history.push('/supplier/availability')}
                className="list-group-item list-group-item-primary d-flex justify-content-between align-items-center">
              <h6>{t('Add your availability')}</h6>
              <div>
                <span className="fa fa-calendar"/>
                {this.state.schedule_status ? <span className="px-2 text-success fa fa-check"/> :
                  <span className="px-2 text-danger fa fa-times"/>}
              </div>
            </li>
            <li onClick={() => this.props.history.push('/supplier/payment')}
                className="list-group-item list-group-item-primary d-flex justify-content-between align-items-center">
              <h6>{t('Add your payment')}</h6>
              <div>
                <span className="fa fa-credit-card"/>
                {this.state.payment_status ? <span className="px-2 text-success fa fa-check"/> :
                  <span className="px-2 text-danger fa fa-times"/>}
              </div>
            </li>
            <li onClick={() => this.props.history.push('/supplier/add/course')}
                className="list-group-item list-group-item-primary d-flex justify-content-between align-items-center">
              <h6>{t('Add course offer')}</h6>
              <div>
                <span className="fa fa-book"/>
                {this.state.course_status ? <span className="px-2 text-success fa fa-check"/> :
                  <span className="px-2 text-danger fa fa-times"/>}
              </div>
            </li>
          </ul>
          <div className='d-flex justify-content-end pt-3'>
            {/*<AppButton color='primary' onClick={this.save} disabled={!this.canSave()}>{'Потвердить и отправить заявку'}</AppButton>*/}
            <ConfirmButton size={'bg'} color={'success'}
                           title={'Вы прошли предквалификационный этап и успешно отправили заявку. Чтобы получить полноценные права репетитора вам надо будет подождать некоторое время пока' +
                           ' наши операторы ознакомятся и проверят вашу заявку как можно скорее.'}
                           onConfirm={() => this.save()} disabled={!this.canSave()}>
              {'Потвердить и отправить заявку'}
            </ConfirmButton>
          </div>
        </Container>
      </Hoc>
    )
  }
}
