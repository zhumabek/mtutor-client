import React, {Component} from 'react';
import {inject, observer} from "mobx-react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  ListGroupItem,
  ListGroup,
  Row,
  Table,
  CardTitle,
  FormGroup,
  Label
} from "reactstrap";
import {translate} from "react-i18next";
import Hoc from "../../../components/Hoc";
import {FGI} from "../../../components/AppInput";
import Input from "../../../components/AppInput";
import DatePicker from "../../../components/DatePicker";
import {MaskedInput} from "../../../components/MaskedInput";
import Switcher from "../../../components/Switcher";
import FileInput from "components/FileInput";
import AppButton from "components/AppButton";
import Select from "components/Select";
import {getApi} from "../../../../../utils/common";

@translate(['common', 'settings', ''])
@inject('supplierStore', 'authStore', 'adminStore') @observer
export default class Profile extends Component {
  state = {
    user_id: null,
    fullname: null,
    birth_date: null,
    inn: null,
    education_name: null,
    education_faculty: null,
    education_degree: null,
    education_degrees: [],
    description_info: null,
    passport_series: null,
    passport_number: null,
    passport_given_date: null,
    passport_given_organ: null,
    transcript: null,
    filePreview: {name: ''}
  };

  componentWillMount() {
    this.props.adminStore.getUser(this.props.authStore.user.id).then(r => {
      console.log(r, 'IMherer');
      this.setState({
        fullname: r.fullname,
        user_id: r.id,
        birth_date: r.birth_date,
        inn: r.inn,
        education_name: r.education_name,
        education_faculty: r.education_faculty,
        education_degree: r.education_degree,
        // education_degrees: [],
        description_info: r.description_info,
        passport_series: r.passport_series,
        passport_number: r.passport_number,
        passport_given_date: r.passport_given_date,
        passport_given_organ: r.passport_given_organ,
        transcript: r.transcript,
      })
      if(r.transcript) this.setState({filePreview: {name: r.transcript.split('\\')[1]}})
    })
  }

  handleFile(state, file) {
    this.state.filePreview = file[0];
    console.log('FILE',file)
    let form = new FormData;
    form.append("transcript", file[0]);
    this.props.supplierStore.uploadFile(form).then(r => {
      this.state[state] = r.fileUrl;
    })
  }

  canSave() {
    let {
      fullname,
      birth_date,
      inn,
      education_name,
      education_faculty,
      education_degree,
      description_info,
      passport_series,
      passport_number,
      passport_given_date,
      passport_given_organ,
      transcript
    } = this.state;
    return fullname &&
      birth_date &&
      inn &&
      education_name &&
      education_faculty &&
      // education_degree &&
      description_info &&
      passport_series &&
      passport_number &&
      passport_given_date &&
      passport_given_organ &&
      transcript
  }

  save = () => {
    let profileInfo = {...this.state}
    this.props.supplierStore.saveApplicationProfile(profileInfo)
      .then(r => this.props.history.push('/supplier/prequalification'))
  }

  render() {
    const {t, authStore} = this.props;
    return (
      <Card>
        <CardBody>
          <Row>
            <Col md={6} xs={12}>
              <CardTitle>{t('Основные данные')}</CardTitle>
              <FGI l='ФИО' lf="3" ls="8" className="my-1" required>
                <Input value={this.state.fullname || ''} placeholder="Введите Фамилию"
                       onChange={e => this.setState({fullname: e.target.value})} disabled/>
              </FGI>
              <FGI l='Дата рождения' lf="3" ls="8" className="my-1" required>
                <DatePicker value={this.state.birth_date}
                            placeholderText={'дд.мм.гггг'}
                            onChange={(value) => this.setState({birth_date: value})}/>
              </FGI>
              <FGI l={t("ИНН")} lf={3} ls={8} className="my-1" required key={0}>
                <MaskedInput mask="99999999999999" value={this.state.inn}
                             callback={inn => this.setState({inn})}/>
              </FGI>
              <FGI l='Раскажи нам о себе' lf="3" ls="8" className="my-1" required>
                <Input type='textarea' value={this.state.description_info || ''} placeholder="Раскажи нам о себе"
                       onChange={e => this.setState({description_info: e.target.value})}/>
              </FGI>
              <CardTitle>{t('Образование')}</CardTitle>
              <FGI l='Учреждение' lf="3" ls="8" className="my-1" required>
                <Input value={this.state.education_name} placeholder="Введите намиенование учреждения"
                       onChange={e => this.setState({education_name: e.target.value})}/>
              </FGI>
              <FGI l='Факультет(Специальность)' lf="3" ls="8" className="my-1" required>
                <Input value={this.state.education_faculty} placeholder="Введите специальность"
                       onChange={e => this.setState({education_faculty: e.target.value})}/>
              </FGI>
              <FGI l='Степень образования' lf="3" ls="8" className="my-1" required>
                <Select options={this.state.education_degrees} placeholder={t('Выберите')}
                        valueKey="id" labelKey="name" value={this.state.education_degree}
                        onChange={education_degree => this.setState({education_degree})}/>
              </FGI>
              <FGI l='Upload your transcript' lf="3" ls="4" className="my-1" required>
                <FileInput fileHandler={file => this.handleFile('transcript', file)} btnLabel={"Выберите файл"}/>
                <div className="py-3">
                  <h4>Наименование файла: <a href={getApi()+this.state.transcript} target={'_blank'}>{this.state.filePreview.name}</a></h4>
                </div>
              </FGI>
            </Col>
            <Col md={6} xs={12}>
              <Row>
                <Col xs={12}>
                  <CardTitle>Паспортные данные</CardTitle>
                  <FGI l='Серия' lf="3" ls="8" className="my-1" required>
                    <Input value={this.state.passport_series} placeholder="Введите серию"
                           onChange={e => this.setState({passport_series: e.target.value})}/>
                  </FGI>
                  <FGI l='№' lf="3" ls="8" className="my-1" required>
                    <Input value={this.state.passport_number} placeholder="Введите №"
                           onChange={e => this.setState({passport_number: e.target.value})}/>
                  </FGI>
                  <FGI l='Дата выдачи' lf="3" ls="8" className="my-1" required>
                    <DatePicker value={this.state.passport_given_date}
                                placeholderText={'дд.мм.гггг'}
                                onChange={(value) => this.setState({passport_given_date: value})}/>
                  </FGI>
                  <FGI l='Орган выд.' lf="3" ls="8" className="my-1" required>
                    <Input value={this.state.passport_given_organ} placeholder="Введите выд. орган"
                           onChange={e => this.setState({passport_given_organ: e.target.value})}/>
                  </FGI>
                </Col>
                <Col xs={12}>
                  <CardTitle>Метод оплаты</CardTitle>
                  <FGI l='Наличными / Кредитная карта' lf="3" ls="8" className="my-1">
                    <div className={"mt-2"}>
                      <Switcher checked={this.state.by_cash}
                                onChange={by_cash => this.setState({by_cash})}/>
                    </div>
                    {this.state.by_cash &&
                    <div>
                      <Label xs={12}>
                        {t('Номер карты')}
                      </Label>
                      <MaskedInput mask="9999999999999999" value={this.state.credit_card_number}
                                   callback={credit_card_number => this.setState({credit_card_number})}/>
                      <FormGroup row className="mt-2">
                        <Col md={8} xs={12}>
                          <DatePicker value={this.state.credit_card_valid}
                                      dateFormat={'MM.YYYY'}
                                      placeholderText={'Срок истечения'}
                                      onChange={(value) => this.setState({credit_card_valid: value})}/>
                        </Col>
                        <Col md={4} xs={12}>
                          <Input type="text" value={this.state.credit_card_cvc} placeholder="CVC"
                                 onChange={e => this.setState({credit_card_cvc: e.target.value})}/>
                        </Col>
                      </FormGroup>
                      <Input type="text" value={this.state.credit_card_name} placeholder="Имя на карте"
                             onChange={e => this.setState({credit_card_name: e.target.value})}/>
                    </div>}
                  </FGI>

                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col xs={12} className="my-2">
              <AppButton color='danger' onClick={() => this.props.history.goBack()}>{t('Отмена')}</AppButton>
              {' '}
              <AppButton color='primary' onClick={this.save} disabled={!this.canSave()}>{t('Сохранить')}</AppButton>
            </Col>
          </Row>
        </CardBody>
      </Card>
    )

  }
}
