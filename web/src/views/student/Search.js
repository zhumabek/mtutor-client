import React, {Component} from 'react';
import {inject, observer} from "mobx-react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  ListGroupItem,
  ListGroup,
  Row,
  Table,
  Container,
  CardImg,
  CardText, CardTitle, Button
} from "reactstrap";
import Select from "components/Select";
import AppButton from "components/AppButton";
import {IMAGES_URL, DEFAULT_AVA} from "utils/common";

@inject('adminStore', 'authStore') @observer
export default class Search extends Component {

  state = {
    subjects: [],
    subject: null,
    tutors: []
  }

  componentWillMount() {
    this.props.adminStore.getAllSubjects().then(r => {
      this.setState({subjects: r})
    });
  }

  search = async () => {
    const tutors = await this.props.adminStore.findTutors(this.state.subject.id);
    this.setState({tutors});
  }

  canSearch() {
    return;
  }

  render() {
    let {tutors} = this.state;
    return (
      <Container>
        <Row className='no-gutters mt-sm-3'>
          <Col xs={12} md={8} className='offset-md-1'>
            <Select options={this.state.subjects} placeholder={'Поиск'} style={{width: '100%'}}
                    valueKey="id" labelKey="name" value={this.state.subject}
                    onChange={subject => this.setState({subject})}/>
          </Col>
          <Col xs={12} md={2}>
            <AppButton style={{width: '100%'}} color='primary' onClick={this.search}>{'Найти'}</AppButton>
          </Col>
        </Row>
        <Row className='mt-sm-5'>
          {this.state.subject ?
            tutors.map(tutor => {
            return (
              <Col xs={12} sm={6} md={3}>
                <Card>
                  <CardBody className='text-center product'>
                    <img src={DEFAULT_AVA} height={'150px'}/>
                    <CardTitle>{tutor.fullname}</CardTitle>
                    <CardText>
                      {tutor.description_info}
                    </CardText>
                    <span className='price'>350сом/час</span>
                    <div className='d-flex justify-content-around'>
                      <div className='buttons'>{5}</div>
                      <div className='buttons'>{1}</div>
                    </div>
                    <div className='d-flex justify-content-around'>
                      <div style={{width: "49%", fontSize: 12}}>часы обучения</div>
                      <div style={{width: "49%", fontSize: 12}}>студенты обучались</div>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            )
          }) : <Col xs={12} className="text-center"><h2>Пожалуйста выберите нужный вам курс или предмет</h2></Col>}
        </Row>
      </Container>

    )

  }
}
