import React, {Component} from 'react';
import {inject, observer} from "mobx-react";
import {Card, CardBody, CardHeader, Col, ListGroupItem, ListGroup, Row, Table} from "reactstrap";
import {translate} from "react-i18next";

@translate(['common', 'settings', '']) 
@inject('supplierStore', 'authStore') @observer
export default class Home extends Component {
  componentWillMount() {

  }

  render() {
    const { t ,authStore} = this.props;
    const {valid, user} = authStore;
    if (!valid) {
      this.props.history.replace('/welcome');
    }
    else if(valid && user.status !== 'confirmed'){
      this.props.history.replace('/supplier/qualification');
    }
    return (
      <div>
        <Row>
          <Col>
            <h1 className="text-center"> {t('Приветствуем вас на странице')}  <br/> <a href={'#/home'}>{t('Mtutor')}</a> </h1>
          </Col>
        </Row>

        <Row>
          <Col md={4} xs={12}>
            <Card>
              <CardHeader className="text-white bg-primary">
                {t('Ищете частного репетитора?')}
              </CardHeader>
              <CardBody>
                <p>{t('Наш сервис соединяет студентов с квалифицированными частными репетиторами.')}</p>
              </CardBody>
            </Card>
          </Col>
          <Col md={4} xs={12}>
            <Card>
              <CardHeader className="text-white bg-primary">{t('Новости')}</CardHeader>
              <CardBody>
                <p>{t('Наш сервис соединяет студентов с квалифицированными частными репетиторами.')}</p>
              </CardBody>
            </Card>
          </Col><Col md={4} xs={12}>
            <Card>
              <CardHeader className="text-white bg-primary">{t('Новости')}</CardHeader>
              <CardBody>
                <p>{t('Наш сервис соединяет студентов с квалифицированными частными репетиторами.')}</p>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )

  }
}
