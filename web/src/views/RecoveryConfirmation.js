import React, {Component} from 'react';
import {inject, observer} from "mobx-react";
import {Card, CardBody, CardFooter, Col, Container, FormText, Row} from "reactstrap";
import Button from "components/AppButton";
import Input, {FormInput} from "components/AppInput";
import {withRouter} from "react-router-dom";

@inject("authStore") @withRouter @observer
export default class RecoveryConfirmation extends Component {

  constructor(props) {
    super(props);
    this.state = {password: null, confirmPassword: null};
  }

  render() {
    return (
      <Container>
        <Row className={"justify-content-center"}>
          <Col md={"6"}>
            <Card>
              <CardBody className="p-4">
                <h4 className="mb-4">Восстановление</h4>

                <Col className="mb-2">
                  <FormInput label="Пароль">
                    <Input type="password"
                           value={this.state.password}
                           onChange={(elem) => {
                             this.setState({password: elem.target.value})
                           }}/>
                    <FormText color="muted">Пароль должен составлять из не меньше 8 символов (букв, цифр)</FormText>
                  </FormInput>
                </Col>

                <Col className="mb-2">
                  <FormInput label="Подтвердите пароль">
                    <Input type="password"
                           value={this.state.confirmPassword}
                           onChange={(elem) => {
                             this.setState({confirmPassword: elem.target.value})
                           }}/>
                  </FormInput>
                </Col>
              </CardBody>

              <CardFooter>
                <Button onClick={() => this.props.authStore.recoveryPassword(this.state).then(r => {
                  this.props.history.push('/home')
                })}>Изменить пароль</Button>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
};
