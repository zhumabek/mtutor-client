import React, {Component} from 'react';
import {inject, observer} from 'mobx-react';
import {FGI} from "components/AppInput";
import {Row, Col, Card, CardHeader, CardBody, Input} from "reactstrap";
import Button from "components/AppButton";
import {translate} from "react-i18next";
import {withRouter} from "react-router-dom";
import {showSuccess} from "utils/messages";

@translate(['common', 'settings', ''])
@withRouter @inject('adminStore') @observer
export default class EditSubject extends Component {
  state = {
    name: "",
    subjectId: null
  };

  componentDidMount() {
    let id = this.props.match.params.id;
    if(id){
      this.props.adminStore.getSubject(id).then(r => this.setState({name: r.name, subjectId: id}));;
    }
  }

  save = () => {
    let params = {
      name: this.state.name
    };
    if(!this.state.subjectId){
      this.props.adminStore.addSubjects(params)
        .then(r => {
          showSuccess('Данные сохранены');
          this.props.history.push("/subjects");
        })
    }
    this.props.adminStore.updateSubject(this.state.subjectId, params)
      .then(r => {
        showSuccess('Данные сохранены');
        this.props.history.push("/subjects");
      })
  };

  render() {
    let {adminStore, t} = this.props;

    return (
      <Card>
        <CardHeader>
          {this.state.subjectId ? "Редактирование существующего курса":"Добавление нового курса"}
        </CardHeader>

        <CardBody className="p-4">

          <Row className={"mb-2"}>
            <Col md="12">
              <FGI l='Наименование курса' lf="2" ls="8">
                <Input value={this.state.name || ''} onChange={e => this.setState({name: e.target.value})}/>
              </FGI>
            </Col>
          </Row>

          <Row>
            <Col md="12">
              <Button onClick={this.save}>
                Сохранить
              </Button>
            </Col>
          </Row>

        </CardBody>
      </Card>
    )
  }
}
