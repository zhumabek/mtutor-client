import React, {Component} from 'react';
import Table from "components/AppTable"
import {inject} from "mobx-react"
import {ConfirmButton} from "components/AppButton";
import {showError, showSuccess} from "utils/messages";
import {Link} from "react-router-dom";
import AppButton from "../../components/AppButton";

@inject("adminStore")
export default class Subjects extends Component {

  state = {};

  componentWillMount() {
    this.getSubjects()
  }

  getSubjects(){
    this.props.adminStore.getAllSubjects().then(subjects => this.setState({subjects}))
  }

  delete(id){
    this.props.adminStore.deleteSubject(id).then(r => this.getSubjects())
  }

  render() {
    let {subjects} = this.state;
    const columns = [
      {
        Header: "Список всех предметов", columns: [
          {Header: "Наименование", accessor: "name"},
          {
            width: 40, filterable: false, sortable: false,
            Cell: (row) => (
              <Link to={'/subject/edit/' + row.original.id} title={'Редактировать'}>
                <i className="fa fa-lg fa-edit mr-2"/>
              </Link>
            )
          },
          {
            width: 40, filterable: false,
            Cell: row => {
                return <ConfirmButton size={'sm'} color={'danger'} title={'Вы действительно хотите удалить?'}
                                      onConfirm={() => this.delete(row.original.id)}>
                  <i className="fa fa-trash"/>
                </ConfirmButton>;
            }
          }
        ]
      }
    ]


    return (
      <div className="animated fadeIn">
        <AppButton onClick={() => this.props.history.push('/subject/add')}>Добавить</AppButton>
        <Table data={subjects}
               columns={columns}/>
      </div>
    )
  }
}
