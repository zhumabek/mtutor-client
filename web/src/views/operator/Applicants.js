import React, {Component} from 'react';
import Table from "components/AppTable"
import {inject} from "mobx-react"
import {ConfirmButton} from "components/AppButton";
import {showError, showSuccess} from "utils/messages";
import {Link} from "react-router-dom";
import AppButton from "../../components/AppButton";
import openSocket from "socket.io-client";
import {Col, Nav, NavItem, NavLink, Row, TabContent, TabPane} from "reactstrap";
import classnames from 'classnames';
import {getApi} from "../../../../utils/common";

@inject("adminStore")
export default class Applicants extends Component {

  state = {
    applicants: [],
    blacklist: [],
    rejected: [],
    approved: [],
    activeTab: '1'
  };

  componentWillMount() {
    this.getPendingApplicants()
    this.getApprovedApplicants()
    this.getRejectedApplicants()
    const socket = openSocket(getApi());
    socket.on('applicants/pending', data => {
      // this.addApplicant(data)
      this.getPendingApplicants()
      this.getApprovedApplicants()
      this.getRejectedApplicants()
    })
  }

  addApplicant(data) {
    let oldApplicants = [...this.state.applicants, data.user];
    this.setState({applicants: oldApplicants})

  }

  getPendingApplicants(){
    this.props.adminStore.getApplicants({status: 'pending'}).then(applicants => this.setState({applicants}))
  }

  getApprovedApplicants(){
    this.props.adminStore.getApplicants({status: 'confirmed'}).then(approved => this.setState({approved}))
  }

  getRejectedApplicants(){
    this.props.adminStore.getApplicants({status: 'rejected'}).then(rejected => this.setState({rejected}))
  }

  delete(id){
    // this.props.adminStore.deleteSubject(id).then(r => this.getSubjects())
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    let {applicants, rejected, approved, blacklist} = this.state;
    const columns = [
      {
        Header: "Список пользователей прошедших предквалификацию", columns: [
          {Header: "№", width: 50,
            Cell: (row) => (
              <p>{row.index + 1}</p>
            )
          },
          {Header: "Наименование", accessor: "fullname"},
          {Header: "Email", accessor: "email"},
          {Header: "Телефон", accessor: "phone"},
          {
            width: 40, filterable: false, sortable: false,
            Cell: (row) => (
              <Link to={'/applicants/edit/' + row.original.id} title={'Просмотр'}>
                <i className="fa fa-lg fa-eye mr-2"/>
              </Link>
            )
          },
          {
            width: 40, filterable: false,
            Cell: row => {
                return <ConfirmButton size={'sm'} color={'danger'} title={'Вы действительно хотите удалить?'}
                                      onConfirm={() => this.delete(row.original.id)}>
                  <i className="fa fa-ban"/>
                </ConfirmButton>;
            }
          }
        ]
      }
    ]


    return (
      <div className="animated fadeIn">
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => {
                this.toggle('1');
                this.getPendingApplicants();
              }}
            >
              Не просмотренные
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '4' })}
              onClick={() => {
                this.toggle('4');
                this.getApprovedApplicants();
              }}
            >
              Одобренные
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => {
                this.toggle('2');
                this.getRejectedApplicants()
              }}
            >
              Отклоненные
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '3' })}
              onClick={() => { this.toggle('3'); }}
            >
              Черный список
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
                <Table data={applicants}
                       columns={columns}/>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="4">
            <Row>
              <Col sm="12">
                <Table data={approved}
                       columns={columns}/>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col xs={12}>
                <Table data={rejected}
                       columns={columns}/>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="3">
            <Row>
              <Col xs={12}>
                <Table data={blacklist}
                       columns={columns}/>
              </Col>
            </Row>
          </TabPane>
        </TabContent>
        {/*<AppButton onClick={() => this.props.history.push('/subject/add')}>Добавить</AppButton>*/}
      </div>
    )
  }
}
