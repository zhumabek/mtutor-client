import React, {Component, Fragment} from 'react';
import {
  Pagination,
  PaginationItem,
  PaginationLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Navbar,
  Nav, NavItem
} from 'reactstrap';
import {action, observable} from "mobx";
import {observer} from "mobx-react";

@observer
export default class EcPagination extends Component {
  @observable items = [];

  componentDidMount() {
    let {total, current} = this.props;
    this.updateItems(total, current);
  }

  componentWillReceiveProps(nextProps) {
    let {total, current} = nextProps;
    this.updateItems(total, current);
  }

  componentWillUnmount() {
    this.reset()
  }

  @action
  updateItems(total, current) {
    let items = [];

    // prev
    items.push({
      disabled: current === 0,
      previous: true,
      id: current - 1
    });

    // pages
    for (let i = 0; i < total; i++) {
      items.push({
        active: i === current,
        title: i + 1,
        id: i
      })
    }

    // next
    items.push({
      disabled: current === (total - 1),
      next: true,
      id: current + 1
    });

    // TODO: add skip (...)

    this.items = items;
  }

  @action
  reset() {
    this.items = [];
  }

  handleClickPage(id) {
    this.props.onChange && this.props.onChange(id);
  }

  handleClickPerPage(perPage) {
    this.props.onChange && this.props.onChange(0, perPage);
  }

  render() {
    if (!this.props.total)
      return null;

    let {perPage, perPageItems} = this.props;

    return (
      <Fragment>
        <Navbar expand="xs">
          <Nav className="ml-auto" navbar>

            <NavItem>
              <Pagination>
                {this.items.map((item, i) => {
                  let {disabled, active, previous, next, title, id} = item;

                  return (
                    <PaginationItem active={active} disabled={disabled} key={i}>
                      <PaginationLink previous={previous} next={next} onClick={() => this.handleClickPage(id)}>
                        {title}
                      </PaginationLink>
                    </PaginationItem>
                  )
                })}

              </Pagination>
            </NavItem>

            <NavItem>&nbsp;</NavItem>

            <UncontrolledDropdown nav color="light" >
              <DropdownToggle caret>
                {perPage}
              </DropdownToggle>
              <DropdownMenu>
                {perPageItems.map(item =>
                  <DropdownItem key={item} onClick={() => this.handleClickPerPage(item)}>{item}</DropdownItem>
                )}
              </DropdownMenu>
            </UncontrolledDropdown>

          </Nav>
        </Navbar>


      </Fragment>
    )
  }
}
