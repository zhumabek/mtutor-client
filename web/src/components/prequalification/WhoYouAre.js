import React, {Component} from "react"
import {inject, observer} from "mobx-react"
import {translate} from "react-i18next";
import AppButton from "../AppButton";
import {Col, Container, Row} from "reactstrap";
import {withRouter} from "react-router-dom";

@translate(['common', 'settings', ''])
@withRouter @inject("supplierStore", "mainStore") @observer
export default class WhoYouAre extends Component {

  render() {
    const {t} = this.props;
    return (
      <div className="container-fluid d-flex flex-column justify-content-between" style={{height: '100%'}}>
        <Row>
          <Col md={6} xs={12} className="offset-md-3">
            <div className="top-info-block">
              <h4>Добро пожаловать в Mtutor!!!</h4>
              <h6>Расскажи нам о себе</h6>
            </div>
          </Col>
        </Row>
        <Row className="flex-grow-1">
          <Col md={6} xs={12} className="offset-md-3 d-flex align-items-center">
        <div className="d-flex flex-column align-items-center justify-content-center flex-grow-1">
          <h1>Я</h1>
          <AppButton block className='btn-pill btn-lg'
                     onClick={() => this.props.history.push(
                       {
                         pathname: '/registration/supplier',
                         state:{
                           role:5
                         }
                       }
                     )}>{t('Студент')}</AppButton>
          <AppButton block className='btn-pill btn-lg'
                     onClick={() => this.props.history.push(
                       {
                         pathname: '/registration/supplier',
                         state:{
                           role:8
                         }
                       }
                     )}>{t('Репетитор')}</AppButton>
        </div>
          </Col>
        </Row>
      </div>
    )
  }
}
