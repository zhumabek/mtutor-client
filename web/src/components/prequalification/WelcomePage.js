import React, {Component} from "react"
import {inject, observer} from "mobx-react"
import {Link} from "react-router-dom";
import {translate} from "react-i18next";
import AppButton from "../AppButton";
import {
  Carousel,
  CarouselCaption,
  CarouselControl,
  CarouselIndicators,
  CarouselItem,
  Col,
  Container,
  Row
} from "reactstrap";
const items = [
  {
    src: 'img/find_tutors.jpg',
    altText: 'Slide 1',
    caption: ''
  },
  {
    src: 'img/meet.jpg',
    altText: 'Slide 2',
    caption: ''
  },
  {
    src: 'img/community.jpg',
    altText: 'Slide 3',
    caption: ''
  }
];
@translate(['common', 'settings', ''])
@inject("supplierStore", "mainStore") @observer
export default class WelcomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
    }

    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({activeIndex: nextIndex});
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({activeIndex: nextIndex});
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({activeIndex: newIndex});
  }

  render() {
    const {activeIndex} = this.state;

    const slides = items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src}
          className='carousel__item'
        >
          <img src={item.src} alt={item.altText} className="carousel__item-image"/>
          <CarouselCaption captionText={item.caption} captionHeader={item.caption}/>
        </CarouselItem>
      );
    });
    const {t} = this.props;
    return (
      <Container>
        <Row>
          <Col xs={12}>
            <div className="d-flex flex-column justify-content-around" style={{height: '100%'}}>
              <Carousel
                activeIndex={activeIndex}
                next={this.next}
                previous={this.previous}
              >
                <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex}/>
                {slides}
                <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous}/>
                <CarouselControl direction="next" directionText="Next" onClickHandler={this.next}/>
              </Carousel>
              <div className="start-action-block pt-5">
                <AppButton block className='btn-pill btn-lg' onClick={() => {
                  this.props.history.push('/choose')
                }}>{t('Начать')}</AppButton>
                <Link to="/login" className="mt-2">{t('У меня уже есть аккаунт')}</Link>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    )
  }
}
