import React, {Component} from 'react';
import {
  Card, CardBody, CardHeader, Col,
  Container,
  Form,
  InputGroup,
  InputGroupAddon,
  InputGroupText, Row
} from "reactstrap";
import Input from "../AppInput";
import Button from "../AppButton";
import {inject, observer} from "mobx-react";
import {withRouter} from 'react-router-dom'
import {translate} from "react-i18next";

@translate(['common', 'settings', '']) @inject('authStore') @observer @withRouter
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {login: null, password: null};
  }

  onSubmitLoginForm = async ev => {
    ev.preventDefault();
    let {authStore} = this.props;

    let r = await authStore.login(this.state.login, this.state.password);
    if (r)
      this.props.history.push('/')
  };

  render() {
    let {t} = this.props;

    return (
      <Container style={{height: '100%'}}>
        <Row>
          <Col xs={12} md={8} className='offset-md-2'>
            {/*<div className="d-flex align-items-center justify-content-center" style={{height:'100%'}}>*/}
              <Card style={{width:'100%'}} className='mt-5'>
                <CardHeader className="text-white bg-primary">
                  {t('Введите Ваш логин и пароль')}
                </CardHeader>
                <CardBody>

                  <Form onSubmit={this.onSubmitLoginForm} className="py-2">
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-user"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="Email" value={this.state.login}
                             onChange={(e) => this.setState({login: e.target.value})}/>
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-lock"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder={t('Пароль')} value={this.state.password}
                             onChange={(e) => this.setState({password: e.target.value})}/>
                    </InputGroup>
                    <Button color="primary" type="submit" className="px-4">{t('Вход')}</Button>
                    <Button color="link" className="px-0" onClick={() => {
                      this.props.history.push("/recovery")
                    }}>{t('Забыли пароль?')}</Button>
                  </Form>
                </CardBody>
              </Card>
            {/*</div>*/}
          </Col>
        </Row>
      </Container>
    )
  }
}

export default Login
