import React, {Component} from 'react';
import {Calendar, momentLocalizer} from "react-big-calendar";
import moment from 'moment';
import PropTypes from 'prop-types'

const localizer = momentLocalizer(moment);

export default class BigCalendar extends Component{
  static propTypes = {
    // onChange: PropTypes.func,
    // selectedDate: PropTypes.object,
    events: PropTypes.array,
    // timesheets: PropTypes.array,
    view: PropTypes.oneOf(['month', 'week', 'day']),
    toolbar: PropTypes.bool
  }

  render(){
    let {events, selectable, view, toolbar, onSelectSlot, defaultDate,
      min, max, onDoubleClickEvent, defaultView} = this.props;


    const formats = {
      dayFormat: 'dddd'
    }
    return (
      <Calendar
        formats={formats}
        localizer={localizer}
        events={events}
        view={view}
        defaulView={defaultView}
        toolbar={toolbar}
        defaultDate={defaultDate}
        selectable={selectable}
        onSelectSlot={onSelectSlot}
        onDoubleClickEvent={onDoubleClickEvent}
        min={min}
        max={max}
      />
    );
  }
}

