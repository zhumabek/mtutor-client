import React from "react";
import {inject, observer} from "mobx-react";
import {action} from "mobx";
import {Input, Label} from "reactstrap";

@inject('mainStore') @observer
export default class Switcher extends React.Component {

/*  constructor(props) {
    super(props);
    this.state = {checked: false};
  }

  componentWillMount() {
    let {model, name, checked} = this.props;
    let item = checked || false;
    if (model && name) {
      item = model[name] || false;
    }
    this.setState({checked: item});
  }
*/

  onChange = (val) => {
    let {model, name, onChange, disabled} = this.props;
    if (disabled) return;
    // let val = !this.state.checked;
    if (model && name) {
      action('set', () => model[name] = val)();
    }
    if (onChange) {
      onChange(val);
    }
    // this.setState({checked: val});
  };

  render() {
    let {model, name, checked} = this.props;
    let item = checked || false;
    if (model && name) {
      item = model[name] || false;
    }

    return (
      <Label className="switch switch-3d switch-primary switch-pill">
        <Input type="checkbox" className="switch-input"
               checked={item}
               disabled={this.props.mainStore.isBusy}
               onChange={(e) => this.onChange(e.target.checked)}/>
        <span className="switch-slider"/>
      </Label>
    )
  }
}
