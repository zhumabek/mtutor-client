import React, {Component} from "react"
import {translate} from "react-i18next";
import Hoc from "components/Hoc";
import {DEFAULT_AVA, IMAGES_URL} from "utils/common";
import {inject, observer} from "mobx-react";
import {Col, DropdownItem, Row} from "reactstrap";
import {NavLink} from "react-router-dom";

@translate(['common', 'settings', ''])
@inject('authStore')@observer
export default class MyAccountHome extends Component {
  constructor(...args) {
    super(...args);

    let {user} = this.props.authStore;

    this.state = {
      fullname: user.fullname,
      avatarPreview: user.data.avatar_img ? (IMAGES_URL + user.data.avatar_img) : DEFAULT_AVA
    };
  }

  render() {
    const {t} = this.props;
    return (
      <Hoc>
        <Row>
          <Col md={4} sm={12} className="d-flex flex-column align-items-center order-0 order-md-1 mt-2">
            <div>
              <img className="Ellipse_1" src={this.state.avatarPreview} alt="avatar"/>
            </div>
            <div>
              <h4 className="text-primary text-center">{this.state.fullname}</h4>
            </div>
          </Col>

        </Row>
        <ul className="list-group pt-3">
          <li onClick={() => this.props.history.push('/myaccount')} className="list-group-item list-group-item-primary d-flex justify-content-between align-items-center">
            <h6>{t('Редактировать профиль')}</h6>
            <span className="fa fa-user"/>
          </li>
          <li onClick={() => this.props.history.push('/home')} className="list-group-item list-group-item-primary d-flex justify-content-between align-items-center">
            <h6>{t('Метод оплаты')}</h6>
            <span className="fa fa-credit-card"/>
          </li>
          <li onClick={() => this.props.history.push('/home')} className="list-group-item list-group-item-primary d-flex justify-content-between align-items-center">
            <h6>{t('Уведомления')}</h6>
            <span className="fa fa-bell"/>
          </li>
          <li className="list-group-item list-group-item-primary d-flex justify-content-between align-items-center">
            <h6>{t('Настройки')}</h6>
            <span className="fa fa-cog"/>
          </li>
          <li onClick={() => {
            this.props.authStore.logout();
            this.props.history.push('/home');
          }} className="list-group-item list-group-item-primary d-flex justify-content-between align-items-center">
            <h6>{t('Выйти')}</h6>
            <span className="fa fa-sign-in-alt"/>
          </li>
        </ul>
      </Hoc>
    )
  }
}
