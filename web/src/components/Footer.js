import React, {Component} from 'react';

class Footer extends Component {
  render() {
    let year = (new Date).getFullYear();
    return (
      <footer className="app-footer ml-0">
        <span className="ml-auto"><a href="mtutor.kg">Mtutor &copy; {year}.</a></span>
      </footer>
    )
  }
}

export default Footer;
