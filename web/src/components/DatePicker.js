import React from 'react';
import {runInAction} from "mobx";
import {inject, observer} from 'mobx-react';
import DPicker from "react-datepicker";
import moment from "moment";
import {InputGroup, InputGroupAddon, InputGroupText} from "reactstrap";

@inject('mainStore') @observer
export default class DatePicker extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const {value, model, name} = this.props;
    if (value) {
      if (model && name)
        if (!model[name]) {
          model[name] = moment(value);
        }
    }
  }

  onChange = (item) => {
    let {model, name, onChange} = this.props;
    if (model && name) {
      runInAction(() => model[name] = item);
    }
    if (onChange) {
      onChange(item);
    }
  };

  render() {
    const {model, name, minDate, maxDate, disabled, placeholder, mainStore, value, addon, icon, ...rest} = this.props;
    let val = value;
    if (model && name && model[name]) {
      val = moment(model[name]);
      val = val.toDate();
    }

    if (val)
      val = moment(val);

    return (
      <InputGroup>
        {addon &&  <InputGroupAddon addonType="prepend">
          <InputGroupText>{icon}</InputGroupText>
        </InputGroupAddon>}

        <DPicker selected={val} className='form-control'
                 onChange={this.onChange}
                 todayButton={"сегодня"}
                 placeholderText={placeholder || 'дд.мм.гггг'}
                 minDate={minDate}
                 maxDate={maxDate}
                 peekNextMonth
                 showMonthDropdown
                 showYearDropdown
                 dropdownMode="select"
                 disabled={disabled || mainStore.isBusy}
                 {...rest}
        />
      </InputGroup>

    )
  }
}
