import React, {Component} from 'react';
import {
  Badge,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  UncontrolledCollapse,
} from 'reactstrap';
import UserDropdown from './UserDropdown';
import Button from "../AppButton";
import {inject, observer} from "mobx-react";
import {Link, NavLink, withRouter} from 'react-router-dom';
import LoginModal from './LoginModal'
import NavMenu from './NavMenu';
import {LOGO_IMG} from 'utils/common';
import CompanyDropdown from './CompanyDropdown';
import LangDropdown from './LangDropdown';
import cx from 'classnames';
import {translate} from "react-i18next";
import Hoc from "components/Hoc";
import BottomNav from "components/Header/BottomNav";

@translate(['common', 'settings', '']) @inject('mainStore', 'authStore', 'menuStore') @withRouter @observer
export default class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {loginModal: false};
  }


  toggleLoginModal = () => {
    this.setState({loginModal: !this.state.loginModal});
  };

  render() {
    const {t, authStore, menuStore} = this.props;
    const {valid, user, notification, isStudent, isTutor} = authStore;
    let headerStyle;
    if (valid && user && user.role) {
      if(isStudent) {
        headerStyle = 'header-color-student'
      }
      else if(isTutor) {
        headerStyle = 'header-color-tutor'
      }
      else{
        headerStyle = ''
      }
    }

    return (
      <Hoc>
        <Navbar color="light" light fixed="top" expand="md" className={cx("border-bottom shadow-sm my-nav", headerStyle)}>
          <NavbarBrand tag={NavLink} to="/" className="py-0" style={{fontWeight:'bold'}}>
            {/*<img className="main-logo" src={LOGO_IMG} alt="E-Market.kg"/>*/}
            <a>Mtutor</a>
          </NavbarBrand>

          <NavbarToggler id="main-nav-toggler"/>
          <UncontrolledCollapse navbar toggler="#main-nav-toggler">

            <NavMenu items={menuStore.items} notifications={notification && notification.count}
                     className="align-items-center mr-auto"/>

            {/*<Button onClick={()=>this.props.mainStore.setMessage('info', 'info')}>info</Button>*/}

            {/*{valid &&
            <Button color="secondary" className="btn-sm mx-2">
              <i className="fa fa-bell"/>
              <Badge pill color="danger">{notification ? notification.count : 0}</Badge>
            </Button>
          }*/}

            {/*<CompanyDropdown user={user}/>*/}

            <LangDropdown/>

            {valid ?
              <UserDropdown {...this.props} user={user}/>
              :
              <div>
                <Button onClick={this.toggleLoginModal}>
                  <i className="fa fa-fw fa-user"/>{t('Войти')}
                </Button>
                <Link to="/choose" className="btn btn-link">{t('Регистрация')}</Link>
              </div>
            }

            <LoginModal isOpen={this.state.loginModal}
                        className={this.props.className}
                        toggle={this.toggleLoginModal}/>

          </UncontrolledCollapse>
        </Navbar>
        {valid &&
           <BottomNav/>
        }
      </Hoc>
    );
  }
}

