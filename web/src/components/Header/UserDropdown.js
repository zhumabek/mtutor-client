import React, {Component} from 'react';
import {UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';
import {NavLink, withRouter} from "react-router-dom";
import {IMAGES_URL, DEFAULT_AVA} from "utils/common";
import Img from 'components/Image';
import {translate} from "react-i18next";

@translate(['common', 'settings', ''])
@withRouter
export default class UserDropdown extends Component {
  render() {
    const {t} = this.props;
    let {user} = this.props;
    let avatar_img = [user.data && user.data.avatar_img && (IMAGES_URL + user.data.avatar_img), DEFAULT_AVA];

    return (
      <UncontrolledDropdown>
        <DropdownToggle nav className="px-1">
          <Img src={avatar_img} className="img-avatar border Ellipse_1" alt=""/>
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem header tag="div" className="text-center">
            <strong>{user.fullname || user.email || user.phone}</strong>
            <br/>
            {user.email}
          </DropdownItem>
          <DropdownItem tag={NavLink} to="/myaccount">
            <i className="fa fa-user"/> {t('Мой аккаунт')}
          </DropdownItem>
          <DropdownItem>
            <i className="fa fa-wrench"/> {t('Настройки')}
          </DropdownItem>
          <DropdownItem onClick={() => {
            this.props.authStore.logout();
            this.props.history.push('/welcome');
          }}>
            <i className="fa fa-lock"/>
            {t('Выйти')}
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    );
  }
}
