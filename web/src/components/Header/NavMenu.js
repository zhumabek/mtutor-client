import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Badge,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  NavItem,
  NavLink as NavLinkRS,
  UncontrolledDropdown
} from 'reactstrap';
import {NavLink} from "react-router-dom";
import {translate} from "react-i18next";

@translate(['common', 'settings', ''])
export default class NavMenu extends Component {
  static propTypes = {
    items: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  };

  static defaultProps = {
    items: [],
  };

  render() {
    let {items, notifications, t, tReady, ...props} = this.props;
    // items = items.filter(e => e.visible);
    return (
      <Nav {...props} navbar>
        {items.map((m, i) =>
          m.items ?
            this.renderDropdown(m.name, m.items, i) :
            this.renderLink(m.name, m.url, i)
        )}
      </Nav>
    )
  }

  renderDropdown(name, items, i) {
    const {t} = this.props;
    return (
      <UncontrolledDropdown nav inNavbar key={i}>
        <DropdownToggle nav caret>
          {t("" + name + "")}
        </DropdownToggle>
        <DropdownMenu right>
          {items.map((mm, i) => (
            <DropdownItem key={i}>
              {this.renderLink(mm.name, mm.url)}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </UncontrolledDropdown>
    )
  }

  renderLink(name, url, i, exact) {
    const {t} = this.props;
    let m = name.match(/^(.*)\{0\}$/); // сообщения{0}
    if (m) {
      name = m[1]; // сообщения
      let {notifications} = this.props;
      name =
        <span>
          {name + " "}
          <Badge pill color="danger">{notifications || 0}</Badge>
        </span>;
    }

    return (
      <NavItem className="px-2 text-center" key={i}>
        <NavLinkRS to={url} tag={NavLink} exact={exact} className="p-1">
          {t("" + name + "")}
        </NavLinkRS>
      </NavItem>
    )
  }
}
