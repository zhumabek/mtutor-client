import React, {Component} from "react"
import {inject, observer} from "mobx-react"
import {translate} from "react-i18next";
import AppButton from "../AppButton";

@translate(['common', 'settings', ''])
@inject("supplierStore", "mainStore") @observer
export default class BottomNav extends Component {

  render() {
    const {t} = this.props;
    return (
      <div className="KGiwt">
        <div className="A8wCM">
          <div className="BvyAW">
            <div className="q02Nz">
              <a className="_0TPg" href="#/">
                <span className="fa fa-home" aria-label="Главная"/>
              </a>
            </div>
            <div className="q02Nz">
              <a className="_0TPg" href="/explore/">
                <span className="fa fa-search" aria-label="Поиск и интересное"/>
              </a>
            </div>
            <div className="q02Nz _0TPg" role="menuitem" tabIndex="0">
              <a className="_0TPg" href="/explore/">
                <span className="fa fa-plus-square" aria-label="Новая публикация"/>
              </a>
            </div>
            <div className="q02Nz">
              <a className="_0TPg " href="#/notifications">
                <span className="fa fa-comment-alt" aria-label="Действия"/>
              </a>
            </div>
            <div className="q02Nz">
              <a className="_0TPg" href="#/profile">
                <span className="fa fa-user" aria-label="Профиль"/>
              </a>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
