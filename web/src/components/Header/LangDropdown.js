import React, {Component} from 'react';
import {inject, observer} from 'mobx-react';
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledDropdown,
} from 'reactstrap';

@inject('mainStore') @observer
export default class LangDropdown extends Component {
  render() {
    const {mainStore} = this.props;
    const {language} = mainStore;
    console.log("language",language)

    return (
      <UncontrolledDropdown>
        <DropdownToggle nav className="px-1">
          <span>
            {language.short_name}
            <img src={language.image} height="23px" className="pl-1"/>
          </span>
        </DropdownToggle>
        <DropdownMenu>
          {mainStore.languages
            .filter(l => l.code !== language.code)
            .map(l =>
              <DropdownItem key={l.code} onClick={() => mainStore.setLanguage(l)}>
                <img src={l.image} height="20px" className="pr-1"/>
                {l.name}
              </DropdownItem>
            )
          }
        </DropdownMenu>
      </UncontrolledDropdown>
    )
  }
}
