import React, {Component} from 'react';
import {
  Form,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader
} from "reactstrap";
import Input from "../AppInput";
import Button from "../AppButton";
import {inject, observer} from "mobx-react";
import {withRouter} from 'react-router-dom'
import {translate} from "react-i18next";

@translate(['common', 'settings', '']) @inject('authStore') @observer @withRouter
class LoginModal extends Component {
  constructor(props) {
    super(props);
    this.state = {login: null, password: null};
  }

  onSubmitLoginForm = async ev => {
    ev.preventDefault();
    let {authStore, toggle} = this.props;

    let r = await authStore.login(this.state.login, this.state.password);
    if (r)
      toggle();
      this.props.history.push('/home')
  };

  render() {
    let {t, isOpen, toggle, className} = this.props;

    return (
      <Modal isOpen={isOpen} toggle={toggle} className={className} backdrop="static">
        <Form onSubmit={this.onSubmitLoginForm}>
          <ModalHeader toggle={toggle}>{t('Введите Ваш логин и пароль')}</ModalHeader>
          <ModalBody>
            <InputGroup className="mb-3">
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className="fa fa-user"/>
                </InputGroupText>
              </InputGroupAddon>
              <Input type="text" placeholder="Email" value={this.state.login}
                     onChange={(e) => this.setState({login: e.target.value})}/>
            </InputGroup>
            <InputGroup className="mb-4">
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className="fa fa-lock"/>
                </InputGroupText>
              </InputGroupAddon>
              <Input type="password" placeholder={t('Пароль')} value={this.state.password}
                     onChange={(e) => this.setState({password: e.target.value})}/>
            </InputGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="primary" type="submit" className="px-4">{t('Вход')}</Button>
            <Button color="link" className="px-0" onClick={() => {
              toggle();
              this.props.history.push("/recovery")
            }}>{t('Забыли пароль?')}</Button>
          </ModalFooter>
        </Form>
      </Modal>
    )
  }
}

export default LoginModal
