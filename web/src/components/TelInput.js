import React, {Component} from 'react';
import IntlTelInput from 'react-intl-tel-input';
import 'react-intl-tel-input/dist/main.css';
import 'file-loader?name=libphonenumber.js!../../../node_modules/react-intl-tel-input/dist/libphonenumber.js';
import {inject, observer} from "mobx-react";

@inject('mainStore') @observer
export default class TelInput extends Component {

  constructor() {
    super();
    this.state = {value: ''};
  }

  handler = (isValid, value, countryData, number) => {
    this.number = number.replace(' ', '');
    this.setState({value});
    // console.log(isValid, value, ':', number);
    this.props.onChange && this.props.onChange(this.number, isValid);
  };

  componentDidMount() {
    this.receiveValue(this.props.value)
  }

  componentWillReceiveProps({value}) {
    this.receiveValue(value);
  }

  receiveValue(value) {
    if (value !== this.number) {
      value = value || '';
      this.setState({value});
    }
  }

  render() {
    const {disabled} = this.props;
    return <IntlTelInput
      preferredCountries={['kg']}
      defaultCountry={'kg'}
      onPhoneNumberChange={this.handler}
      format={true}
      nationalMode={false}
      value={this.state.value}
      css={['intl-tel-input', 'form-control']}
      utilsScript={'/libphonenumber.js'}
      disabled={disabled || this.props.mainStore.isBusy}
    />
  }
}
