import React, {Component} from 'react';
import Home from "./views/Home";
import {Redirect, Route, Switch} from 'react-router-dom';

import Roles from "./views/admin/Roles";
import Spr from "./views/operator/Spr";
import Recovery from "./views/Recovery";
import RecoveryConfirmation from "./views/RecoveryConfirmation";


import Menus from "./views/admin/Menus";
import Users from './views/operator/Users';
import Subjects from './views/operator/Subjects';
import EditSubject from './views/operator/EditSubject';



import PasswordRecovery from "./views/supplier/PasswordRecovery";
import MyAccount from "./views/supplier/MyAccount";

import EditUser from "./views/operator/EditUser";
import BlackList from "./views/BlackList";
import WelcomePage from "./components/prequalification/WelcomePage";
import WhoYouAre from "./components/prequalification/WhoYouAre";
import Login from "./components/prequalification/Login";
import MyAccountHome from "components/myAccount";
import AddAvailability from "./views/supplier/prequalification/AddAvailability";
import SupplierQualification from "./views/supplier/prequalification";
import Profile from "./views/supplier/prequalification/Profile";
import SupplierRegistration from "./views/supplier/Registration";
import SupplierRegistrationConfirmation from "./views/supplier/Confirmation";
import CourseOffer from "./views/supplier/prequalification/CourseOffer";
import Applicants from "./views/operator/Applicants";
import ViewApplicants from "./views/operator/ViewApplicants";
import Search from "./views/student/Search";

export default class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home}/>

        <Route path="/spr" component={Spr}/>
        <Route path="/users" component={Users}/>
        <Route path="/subjects" component={Subjects}/>
        <Route path="/subject/add" component={EditSubject}/>
        <Route path="/subject/edit/:id" component={EditSubject}/>
        <Route path="/user/edit/:id" component={EditUser}/>
        <Route exact path="/applicants" component={Applicants}/>
        <Route exact path="/applicants/edit/:id" component={ViewApplicants}/>
        <Route path="/roles" component={Roles}/>
        <Route path="/menus" component={Menus}/>
        <Route exact path="/passwordRecovery" component={PasswordRecovery}/>
        <Route exact path="/recovery" component={Recovery}/>
        <Route exact path="/recovery/confirm" component={RecoveryConfirmation}/>

        <Route path="/myaccount" component={MyAccount}/>



        <Route path="/home/blacklist" exact component={BlackList} />
        <Route path="/find/tutor" exact component={Search} />

        <Route path='/supplier/availability' exact component={AddAvailability} />
        <Route path='/supplier/qualification' exact component={SupplierQualification} />
        <Route path='/supplier/profile' exact component={Profile} />
        <Route path='/supplier/add/course' exact component={CourseOffer} />
        <Route path='/registration/supplier' exact component={SupplierRegistration} />
        <Route path='/registration/supplier/password' exact component={SupplierRegistrationConfirmation} />

        <Route path="/welcome" component={WelcomePage}/>
        <Route path="/choose" component={WhoYouAre}/>
        <Route path="/login" component={Login}/>
        <Route path="/profile" component={MyAccountHome}/>

        <Redirect from="*" to="/"/>
      </Switch>
    );
  }
}
