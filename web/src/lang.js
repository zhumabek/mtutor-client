import moment from 'moment';
import numeral from "numeral";
import 'utils/locales/numeral-locale-ru-som';
import 'moment/locale/ru';
import {FORMAT_MONEY} from 'utils/common';
import validatorjs from "validatorjs";
import validator_kg from 'utils/locales/kg/validatorjs';


let defaultLang = 'ru';

moment.locale(defaultLang);

numeral.locale(defaultLang);
numeral.defaultFormat(FORMAT_MONEY);

validatorjs.useLang(defaultLang);
validatorjs.setMessages('kg', validator_kg);
