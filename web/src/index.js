import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'mobx-react';
import 'utils/helpers';
import './lang';

import Root from './root';
import {createStorage} from "utils/LocalStorage";
import stores from "stores";
import 'nodelist-foreach-polyfill';

import 'react-table/react-table.css';
import 'react-select/dist/react-select.css';
import 'react-datepicker/dist/react-datepicker.css';
import '@fortawesome/fontawesome-free/css/all.css';
import 'react-loading-bar/dist/index.css';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import '../scss/style.scss';


createStorage(window.localStorage);

const rootEl = document.getElementById('root');

function renderRoot() {
  render((
    <Provider {...stores}>
      <Root/>
    </Provider>
  ), rootEl);
}

renderRoot();
if (module.hot) {
  module.hot.accept();
}
